package cn.smallbun.screw.core.engine.excel.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * 数据库信息
 *
 * @author fanglei
 * @date 2021/08/10
 **/
@Data
@HeadFontStyle(fontHeightInPoints = 12)
@HeadRowHeight(20)
@ContentStyle(horizontalAlignment = HorizontalAlignment.LEFT)
@HeadStyle(horizontalAlignment = HorizontalAlignment.LEFT)
public class DBInfoExcelModel {
    
    @ExcelProperty("数据库名")
    @ColumnWidth(10)
    private String dbName;
    
    @ExcelProperty("文档版本")
    @ColumnWidth(30)
    private String version;
    
    @ExcelProperty("文档描述")
    @ColumnWidth(30)
    private String description;
}
