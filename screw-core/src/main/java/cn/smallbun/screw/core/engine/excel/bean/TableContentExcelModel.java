package cn.smallbun.screw.core.engine.excel.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

@Data
@HeadFontStyle(fontHeightInPoints = 12)
@HeadRowHeight(20)
@ContentStyle(horizontalAlignment = HorizontalAlignment.LEFT)
@HeadStyle(horizontalAlignment = HorizontalAlignment.LEFT)
public class TableContentExcelModel {

    @ExcelProperty("序号")
    @ColumnWidth(10)
    private Integer numberNo;

    @ExcelProperty("表名")
    @ColumnWidth(30)
    private String tableName;

    @ExcelProperty("说明")
    @ColumnWidth(30)
    private String remark;
}
