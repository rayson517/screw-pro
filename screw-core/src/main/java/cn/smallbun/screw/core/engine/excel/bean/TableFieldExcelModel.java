package cn.smallbun.screw.core.engine.excel.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * excel 表结构导出模板
 * @author Fang Lei
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@HeadFontStyle(fontHeightInPoints = 12)
@HeadRowHeight(20)
@ContentStyle(horizontalAlignment = HorizontalAlignment.LEFT)
@HeadStyle(horizontalAlignment = HorizontalAlignment.LEFT)
public class TableFieldExcelModel {
    
    @ExcelProperty({"数据列信息", "序号"})
    @ColumnWidth(10)
    private Integer numberNo;
    
    @ExcelProperty({"数据列信息", "名称"})
    @ColumnWidth(30)
    private String cloumnName;

    @ExcelProperty({"数据列信息", "数据类型"})
    @ColumnWidth(20)
    private String dataType;
    
    @ExcelProperty({"数据列信息", "长度"})
    @ColumnWidth(20)
    private String dataLength;
    
    @ExcelProperty({"数据列信息", "小数位"})
    @ColumnWidth(20)
    private String dataScale;
    
    @ExcelProperty({"数据列信息", "允许空值"})
    @ColumnWidth(10)
    private String check;
    
    @ExcelProperty({"数据列信息", "主键"})
    @ColumnWidth(10)
    private String pk;

    @ExcelProperty({"数据列信息", "默认值"})
    @ColumnWidth(10)
    private String defaultValue;
    
    @ExcelProperty({"数据列信息", "说明"})
    @ColumnWidth(30)
    private String remark;
    
}
